package com.example.gbusolutions.konstella.constants;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.gbusolutions.konstella.fragments.GBU_EDU_AnnouncementFragment;
import com.example.gbusolutions.konstella.fragments.GBU_EDU_EventsFragment;
import com.example.gbusolutions.konstella.fragments.GBU_EDU_MoreFragment;
import com.example.gbusolutions.konstella.fragments.GBU_EDU_SignUpFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
 
    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }
 
    @Override
    public Fragment getItem(int position) {
 
        switch (position) {
            case 0:
                GBU_EDU_AnnouncementFragment anccFrag = new GBU_EDU_AnnouncementFragment();
                return anccFrag;
            case 1:
                GBU_EDU_SignUpFragment gbu_edu_signUpFragment = new GBU_EDU_SignUpFragment();
                return gbu_edu_signUpFragment;
            case 2:
                GBU_EDU_EventsFragment gbu_edu_eventsFragment = new GBU_EDU_EventsFragment();
                return gbu_edu_eventsFragment;
            case 3:
                GBU_EDU_MoreFragment gbu_edu_moreFragment = new GBU_EDU_MoreFragment();
                return gbu_edu_moreFragment;
            default:
                return null;
        }
    }
 
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}