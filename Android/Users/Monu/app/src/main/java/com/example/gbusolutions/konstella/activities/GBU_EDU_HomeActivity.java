package com.example.gbusolutions.konstella.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gbusolutions.konstella.R;
import com.example.gbusolutions.konstella.constants.GBU_EDU_LoadFragment;
import com.example.gbusolutions.konstella.fragments.GBU_EDU_DirectoryFragment;
import com.example.gbusolutions.konstella.fragments.GBU_EDU_GroupsFragment;
import com.example.gbusolutions.konstella.fragments.GBU_EDU_MeFragment;
import com.example.gbusolutions.konstella.fragments.GBU_EDU_SchoolFragment;

/**
 * Created by gbusolutions on 03-05-2016.
 */
public class GBU_EDU_HomeActivity extends Activity implements View.OnClickListener{
    public static int containerFrag;
    GBU_EDU_LoadFragment loadFragment;
    TextView headerText;
    ImageView groupImg,schoolImg,dirImg,meImg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_act);
        allIds();
        myListener();
        loadFragment.loadNewFargment(new GBU_EDU_GroupsFragment(),GBU_EDU_HomeActivity.this);
    }

    public void allIds(){
        headerText = (TextView)findViewById(R.id.header_text);
        groupImg = (ImageView)findViewById(R.id.group_img);
        schoolImg = (ImageView)findViewById(R.id.school_img);
        dirImg = (ImageView)findViewById(R.id.dir_img);
        meImg = (ImageView)findViewById(R.id.me_img);
        containerFrag = R.id.load_fragments;
        loadFragment = new GBU_EDU_LoadFragment();
    }
    public void myListener() {
        groupImg.setOnClickListener(this);
        schoolImg.setOnClickListener(this);
        dirImg.setOnClickListener(this);
        meImg.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.group_img:
                loadFragment.loadNewFargment(new GBU_EDU_GroupsFragment(), GBU_EDU_HomeActivity.this);
                changeImages();
                groupImg.setImageResource(R.drawable.group_icon_red);
                headerText.setText("Groups");
                break;
            case R.id.school_img:
                loadFragment.loadNewFargment(new GBU_EDU_SchoolFragment(),GBU_EDU_HomeActivity.this);
                changeImages();
                schoolImg.setImageResource(R.drawable.school_red);
                headerText.setText("Delhi Public School");
                break;
            case R.id.dir_img:
                loadFragment.loadNewFargment(new GBU_EDU_DirectoryFragment(),GBU_EDU_HomeActivity.this);
                changeImages();
                dirImg.setImageResource(R.drawable.directory_red);
                headerText.setText("Delhi Public School");
                break;
            case R.id.me_img:
                loadFragment.loadNewFargment(new GBU_EDU_MeFragment(), GBU_EDU_HomeActivity.this);
                changeImages();
                meImg.setImageResource(R.drawable.me_red);
                headerText.setText("Me");
                break;
        }
    }

    public void changeImages() {
        groupImg.setImageResource(R.drawable.group_icon);
        schoolImg.setImageResource(R.drawable.school);
        dirImg.setImageResource(R.drawable.directory);
        meImg.setImageResource(R.drawable.me);
    }
}
