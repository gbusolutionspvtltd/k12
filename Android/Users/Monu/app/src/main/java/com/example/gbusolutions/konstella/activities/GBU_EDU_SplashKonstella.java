package com.example.gbusolutions.konstella.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.gbusolutions.konstella.R;

/**
 * Created by gbusolutions on 03-05-2016.
 */
public class GBU_EDU_SplashKonstella extends Activity{
    private static final int SPLASH_TIME = 3000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_konstella);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(GBU_EDU_SplashKonstella.this, GBU_EDU_HomeActivity.class);
                startActivity(i);
                finish();
            }
        },SPLASH_TIME);
    }
}
