package com.example.gbusolutions.konstella.constants;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import com.example.gbusolutions.konstella.activities.GBU_EDU_HomeActivity;

/**
 * Created by gbusolutions on 03-05-2016.
 */
public class GBU_EDU_LoadFragment {
    public void loadNewFargment(Fragment fragment,Activity activity){
        FragmentManager fragmentManager = activity.getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(GBU_EDU_HomeActivity.containerFrag, fragment);
        fragmentTransaction.commit();
    }
}
