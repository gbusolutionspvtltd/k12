package com.example.gbusolutions.konstella.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.gbusolutions.konstella.R;

/**
 * Created by gbusolutions on 06-05-2016.
 */
public class GBU_EDU_EventsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.events_fragment,container,false);
        return view;
    }
}
