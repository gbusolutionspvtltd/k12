<?php

namespace App\Http\Controllers;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Hash;
use App\Childinfo;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $data = DB::table('users')
            ->join('child_info', 'child_info.user_id', '=','users.id')
            ->where('users.id','=',$user->id)
            ->select('firstname','lastname','childs_firstname','childs_lastname','school_id','teachers_rooms_id')
            ->get();
    
        $sec_data=DB::table('schools')->select('school_names')->where('id',$data[0]->school_id)->first();
        
        $third_data=DB::table('teachers_rooms')->select('room_no','teachers_name')->where('id',$data[0]->teachers_rooms_id)->first();
        
        $final=array('first_name'=>$data[0]->firstname,'last_name'=>$data[0]->lastname,'school_name'=>$sec_data->school_names,'room_no'=>$third_data->room_no,'teacher_name'=>$third_data->teachers_name,'childs_first_name'=>$data[0]->childs_firstname,'childs_last_name'=>$data[0]->childs_lastname);
        
        return view('app_page', ['child' => $final]);
    }

   
    public function children(){

        $user = Auth::user();
        $data = DB::table('users')
            ->join('child_info', 'child_info.user_id', '=','users.id')
            ->where('users.id','=',$user->id)
            ->select('firstname','lastname','childs_firstname','childs_lastname','school_id','teachers_rooms_id')
            ->get();
    
        $sec_data=DB::table('schools')->select('school_names')->where('id',$data[0]->school_id)->first();
        
        $third_data=DB::table('teachers_rooms')->select('room_no','teachers_name')->where('id',$data[0]->teachers_rooms_id)->first();
        
        $final=array('first_name'=>$data[0]->firstname,'last_name'=>$data[0]->lastname,'school_name'=>$sec_data->school_names,'room_no'=>$third_data->room_no,'teacher_name'=>$third_data->teachers_name,'childs_first_name'=>$data[0]->childs_firstname,'childs_last_name'=>$data[0]->childs_lastname);
        
        return view('children', ['child' => $final]);
    }

    public function upload_profile(){
    
         $image = Input::file('profile_picture');
         $rules = array(
         'profile_picture' => 'required|image',
        
    );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
           return Redirect::action('HomeController@personal_settings')->withErrors($validator);
        }
        $user = Auth::user();
        $imagename = $user->id . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path() . '/profile_pictures';
        if(!$image->move($destinationPath, $imagename)) {
            return Redirect::action('HomeController@personal_settings')->withMessage("Oops! Error in Image Upload!");
        }
        $user->profile_picture = $imagename;
        $user->save();
        return Redirect::action('HomeController@personal_settings')->withMessage("Image Uploaded Successfully!");
        
    }

    public function personal_settings(){
        
        $data = User::all();
        $user = Auth::user();
        
        $data = DB::table('users')
            ->join('child_info', 'child_info.user_id', '=','users.id')
            ->where('users.id','=',$user->id)
            ->select('firstname','lastname','childs_firstname','childs_lastname','school_id','teachers_rooms_id')
            ->get();
    
        $sec_data=DB::table('schools')->select('school_names')->where('id',$data[0]->school_id)->first();
        
        $third_data=DB::table('teachers_rooms')->select('room_no','teachers_name')->where('id',$data[0]->teachers_rooms_id)->first();
        
        $final=array('first_name'=>$data[0]->firstname,'last_name'=>$data[0]->lastname,'school_name'=>$sec_data->school_names,'room_no'=>$third_data->room_no,'teacher_name'=>$third_data->teachers_name,'childs_first_name'=>$data[0]->childs_firstname,'childs_last_name'=>$data[0]->childs_lastname);



        return view('account_settings', ['data' => $data , 'child' => $final]);
    }

    public function changePassword() 
{
    $user = Auth::user();
    $rules = array(
        'old_password' => 'required|alphaNum|min:6',
        'password' => 'required|alphaNum|min:6|confirmed'
    );

    $validator = Validator::make(Input::all(), $rules);

    if ($validator->fails()) 
    {
        return Redirect::action('HomeController@personal_settings',$user->id)->withErrors($validator);
    } 
    else 
    {
        if (!Hash::check(Input::get('old_password'), $user->password)) 
        {
            return Redirect::action('HomeController@personal_settings',$user->id)->withErrors('Your old password does not match!');
        }
        else
        {
            $user->password = Hash::make(Input::get('password'));
            $user->save();
            return Redirect::action('HomeController@personal_settings',$user->id)->withMessage("Your password have been changed!");
        }
    }
}
    
}