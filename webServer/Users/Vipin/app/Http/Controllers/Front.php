<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use App\JoinRequest;
use App\Json;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

class Front extends Controller
{
    public function json(){
      $jr = JoinRequest::all();
      $data = new Json;
      $data->json = json_encode($jr);
      $data->save();
    }
    
    public function home(){
        if(Auth::check()) 
            return Redirect::to('/app')->with('flash_notice', 'You are already logged in!');

      return view('welcome');
    }

    public function joinrequest()
    {
        return view('joinrequest');
    }

    public function saverequest()
    {
    	$validator = Validator::make(Input::all(), JoinRequest::$rules);

    	if ($validator->fails()) {

        // get the error messages from the validator
        $messages = $validator->messages();

        // redirect our user back to the form with the errors from the validator
        return Redirect::back()->withErrors($validator);

    }

        $data = new JoinRequest;
        $data->email = Input::get('email');
	    $data->firstname = Input::get('firstname');
	    $data->lastname = Input::get('lastname');
	    $data->school = Input::get('school');
	    $data->childs_firstname = Input::get('childs_firstname');
	    $data->childs_lastname = Input::get('childs_lastname');
	    $data->relationship_to_child = Input::get('relationship_to_child');
	    $data->classroom = Input::get('classroom');
	    $data->note = Input::get('note');
	    $data->save();

     Session::flash('message', 'Request Sent Successfully!');
     return Redirect::back();
    
}
}
