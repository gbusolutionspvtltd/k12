<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
      
     Route::get('/', 'Front@home');
     Route::post('/', 'Front@saverequest');
     Route::get('/joinrequest', 'Front@joinrequest');
     Route::get('/json', 'Front@json');

});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/app', 'HomeController@index');

    Route::get('/app/settings/personal', 'HomeController@personal_settings');
    Route::get('/app/settings/children', 'HomeController@children');
    Route::post('app/settings/personal/change_password', 'HomeController@changePassword');
    Route::post('app/settings/personal/change_picture', 'HomeController@upload_profile');
    


});
