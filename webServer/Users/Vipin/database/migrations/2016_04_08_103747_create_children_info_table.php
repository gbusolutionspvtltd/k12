<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildrenInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->unsigned();
            $table->integer('teachers_rooms_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('teachers_rooms_id')->references('id')->on('teachers_rooms');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('childs_firstname');
            $table->string('childs_lastname');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('child_info');
    }
}
