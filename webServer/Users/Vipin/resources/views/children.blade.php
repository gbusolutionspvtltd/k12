 @extends('layouts.app_main')
 @section('content')
 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Name
                                     <!--MAIN CONTENT WILL BE HERE! -->
            
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Your Children</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-3">
              
             @if ($errors->has())
                <div id="alert_message" class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>        
                    @endforeach
                </div>
         @endif

         @if (Session::has('message'))
                <div id="alert_message" class="alert alert-success">{{ Session::get('message') }}</div>
         @endif
    <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">Your Children!</div>
                <div class="panel-body">
    <form class="form-horizontal" role="form" method="post" action="{{ url('/') }}">
                 {!! csrf_field() !!}
                  
                  
                  <div class="form-group">
                    <label class="col-md-4 control-label"
                          for="firstname" >First Name</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control"
                            name="firstname" value="{{ Auth::user()->name }}" required/>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-4 control-label"
                          for="firstname" >Last Name</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control"
                            name="firstname" value="{{ Auth::user()->name }}" required/>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label  class="col-md-4 control-label"
                              for="email">Relationship To Child</label>
                    <div class="col-md-6">
                        <input type="email" class="form-control" 
                            name="email" value="{{ Auth::user()->email }}" readonly />
                    </div>
                  </div>
                 
                   <div class="form-group">
                    <label  class="col-md-4 control-label"
                              for="email">Family Members</label>
                    <div class="col-md-6">
                        <input type="email" class="form-control" 
                            name="email" placeholder="Enter your phone no." required />
                    </div>
                  </div>
                  <div class="form-group">
                    <label  class="col-md-4 control-label"
                              for="email">School</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" 
                            name="email" placeholder="My School Name" readonly />
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-md-4 control-label"
                          for="childsfirstname" >Classroom</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control"
                            name="childs_firstname" placeholder="Enter your street" required/>
                    </div>
                  </div>
            
                
                 
                    <div class="form-group">
                            <div class="col-md-4 col-md-offset-4">     
                    <button type="submit" class="btn btn-danger" >Delete</button>
                    <button type="submit" class="btn btn-primary" >Add Child</button>
                    <button type="submit" class="btn btn-success" >Save</button>
                           </div>
                    </div>

                  </div>
                
                </form>

                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Change Password</h4>
          </div>
          <div class="modal-body">
  
                <form class="form-horizontal" role="form" method="post" action="{{ url('app/settings/personal/change_password') }}">
                 {!! csrf_field() !!}
                  
                  <div class="form-group">
                    <label  class="col-sm-4 control-label"
                              for="email">Old Password</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" 
                        name="old_password" placeholder="Enter your old password" required />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label"
                          for="firstname" >New Password</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control"
                            name="password" placeholder="Enter your new password" required/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label"
                          for="lastname" >Confirm New Password</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control"
                            name="password_confirmation" placeholder="Enter your new password again" required/>
                    </div>
                  </div>
                  <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Submit</button>
                  </div>
                  </form>
                  </div>
                  </div>
                  </div>
                  </div>

                  

                <style type="text/css">
                  .alert {
                  	   margin-left: 20px;
                       width: 746px;
                    }

                </style>

                <script type="text/javascript">
                window.setTimeout(function() {
                $("#alert_message").fadeTo(500, 0).slideUp(500, function(){
                $(this).remoe(); 
              });
            }, 3000);
                </script>

                </div>
                </div>
                </div>
                </div>
                </div>
            </div><!-- /.col -->
            
          </div><!-- /.row -->v
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2016 <a href="http://gbusolutions.com">GBU Solutions</a>.</strong> All rights reserved.

      </footer>
      @endsection