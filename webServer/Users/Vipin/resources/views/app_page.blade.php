 @extends('layouts.app_main')
 @section('content')
 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            School - Updates
                             <!--MAIN CONTENT WILL BE HERE! -->
            {{$child['room_no']}}
            
           
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">School Updates</li>
          </ol>
        </section>
       
        <!-- Main content -->
        <section class="content">
          <div class="row">
           
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-body no-padding">
                  <!-- THE CALENDAR -->
                  <div id="calendar"></div>
                </div><!-- /.box-body -->
              </div><!-- /. box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        
        <strong>Copyright &copy; 2016 <a href="http://gbusolutions.com">GBU Solutions</a>.</strong> All rights reserved.
      </footer>
      @endsection