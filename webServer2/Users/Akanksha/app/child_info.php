<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class child_info extends Model
{
     protected $fillable = ['school_id', 'teachers_rooms_id', 'user_id', 'childs_firstname', 'childs_lastname'];
	
    protected $table = 'child_info';

     public static $rules = array(                               // just a normal required validation
        'school_id'  => 'required',   
        'teachers_rooms_id'        => 'required',                      
        'user_id'    => 'required',
        'childs_firstname' => 'required|Alpha',
        'childs_lastname'  => 'required|Alpha',    
        
                                                                 
    );
}
