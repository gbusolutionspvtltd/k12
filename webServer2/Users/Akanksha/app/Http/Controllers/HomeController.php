<?php

namespace App\Http\Controllers;
use DateTime;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth; 
use Session;
use DB;
use App\JoinRequest;
use Illuminate\Support\Facades\Input;
use App\studetail;
use Redirect;
use App\teachers_room;
use App\User;
use mPDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowAppPage()
    {
		
		$id = DB::table('users')->select('id')->where('email', Auth::user()->email)->first();
		$data = DB::table('users')
            ->join('child_info', 'child_info.user_id', '=','users.id')
            ->where('users.id','=',$id->id)
            ->select('firstname','lastname','childs_firstname','childs_lastname','school_id','teachers_rooms_id')
            ->get();
	
		$sec_data=DB::table('schools')->select('school_names')->where('id',$data[0]->school_id)->first();
		
		$third_data=DB::table('teachers_rooms')->select('room_no','teachers_name')->where('id',$data[0]->teachers_rooms_id)->first();
		
		$final=array('parent_first_name'=>$data[0]->firstname,'parent_last_name'=>$data[0]->lastname,'schoolname'=>$sec_data->school_names,'class'=>$third_data->room_no,'teacher_name'=>$third_data->teachers_name,'stu_first_name'=>$data[0]->childs_firstname,'stu_last_name'=>$data[0]->childs_lastname);
		
		\Session::put('blog',$final);
		

		
				$events=DB::table('events')->select('title','description','start','backgroundColor','borderColor')->get();
			//	echo 	json_encode($events);
	
	 	return view('calender')->with('content',json_encode($events));
		//return view('calender');
	
	
    }
   
   
    public function childstatus()
	{
	//it counts childs and create tabs in child blade..
							
							
			$result = DB::table('users')
           		 ->join('child_info', 'child_info.user_id', '=','users.id')
			  ->where('email','=',Auth::user()->email)
           	 ->select('child_info.id','school_id','childs_firstname','childs_lastname','relationship_to_child','teachers_rooms_id',DB::raw('count(user_id) as child'))
			->get();
									
	     $classroom=DB::table('teachers_rooms')->select('room_no','teachers_name')->where([['id',$result[0]->teachers_rooms_id],['school_id',$result[0]->school_id]])->first();
							
		 return view('childstatus')->with('data',$result)->with('classroom',$classroom);
					 
}
	
	
	public function addchild()
	{
		$sch_id=DB::table('schools')->select('id')->where('school_names', Session::get('blog.schoolname'))->first();
			$parent_id=DB::table('users')->select('id')->where('email', Auth::user()->email)->first();
			$class_id=DB::table('teachers_rooms')->select('id')->where([['school_id',$sch_id->id],['room_no',$_REQUEST['sec_child_class']]])->first();
			DB::table('child_info')->insert([
    			'school_id' => $sch_id->id,
    			'user_id' => $parent_id->id,
				'teachers_rooms_id' => $class_id->id,
				'childs_firstname' => $_REQUEST['sec_child_firstname'],
				'childs_lastname' => $_REQUEST['sec_child_lastname']
										]);
			 return Redirect::back();
	}
	
	public function filldata($id)
	{
				$result = DB::table('users')
           					->join('child_info', 'child_info.user_id', '=','users.id')
							 ->where([['email',Auth::user()->email],['child_info.id',$id]])
           					 ->select('child_info.id','school_id','childs_firstname','childs_lastname','relationship_to_child','teachers_rooms_id')
							 ->get();
								
		$classroom=DB::table('teachers_rooms')->select('room_no','teachers_name')->where([['id',$result[0]->teachers_rooms_id],['school_id',$result[0]->school_id]])->first();
		 return view('childstatus')->with('data',$result)->with('classroom',$classroom);
	
	}
	
	public function updatechild()
	{
		
		
			$id=DB::table('child_info')->select('school_id')->where('id',Input::get('id'))->first();
		
			$class_id=DB::table('teachers_rooms')->select('id','teachers_name')->where([['school_id',$id->school_id],['room_no',Input::get('classroom')]])->first();
				
				$final=array(
				'childs_firstname'=>Input::get('firstname'),
				'childs_lastname'=>Input::get('lastname'),
				'teachers_rooms_id'=>$class_id->id
				);
				
				DB::table('child_info')
           		 ->where('id', Input::get('id'))
           		 ->update($final);
				 
				 DB::table('users')
           		 ->where('email', Auth::user()->email)
           		 ->update(['relationship_to_child'=>Input::get('relation')]);
				 Session::flash('message', 'Data Has Been Updated Successfully!');
				return Redirect::back();
			}	
			
		public function deletechild($id)
			{
				DB::table('child_info')->where('id', $id)->delete();
					return Redirect::to('childstatus');
			}		
		
		public function school_directory()
		{ 
		
			$id = DB::table('users')->select('id')->where('email',Auth::user()->email)->first();
			$sch_id = DB::table('child_info')->select('school_id')->where('user_id',$id->id)->first();
			$room=DB::table('teachers_rooms')->select('room_no','id')->where('school_id',$sch_id->school_id)->get();
			$result = DB::table('users')
           					->join('child_info', 'child_info.user_id', '=','users.id')
							 ->where('teachers_rooms_id',$room[0]->id)
           					 ->select('users.id','firstname','lastname','childs_firstname','childs_lastname')
							 ->get();			
			 $stu_room=DB::table('teachers_rooms')->select('room_no')->where('id',$room[0]->id)->first();
			 return view('school_directory')->with('room',$room)->with('user_info',$result)->with('stu_room',$stu_room);
		}
		public function showdata($id)
		{
			$user_id = DB::table('users')->select('id')->where('email',Auth::user()->email)->first();
			$sch_id = DB::table('child_info')->select('school_id')->where('user_id',$user_id->id)->first();
			$room=DB::table('teachers_rooms')->select('room_no','id')->where('school_id',$sch_id->school_id)->get();
			$result = DB::table('users')
           					->join('child_info', 'child_info.user_id', '=','users.id')
							 ->where('teachers_rooms_id',$id)
           					 ->select('users.id','firstname','lastname','childs_firstname','childs_lastname')
						 ->get();
					
			 $stu_room=DB::table('teachers_rooms')->select('room_no')->where('id',$id)->first();
			 return view('school_directory')->with('room',$room)->with('user_info',$result)->with('stu_room',$stu_room);
		}
		public function getdata($id)
		{
			$info=DB::table('users')->select('phone_no_1','street','city','country','email','firstname','lastname','relationship_to_child')->where('id',$id)->first();
			$address=$info->street." ".$info->city.",".$info->country;
			
			//$child_info=DB::table('child_info')->select('childs_firstname','childs_lastname','teachers_rooms_id')->where('user_id',$id)->get();
			$child_info = DB::table('child_info')
           					->join('teachers_rooms', 'child_info.teachers_rooms_id', '=','teachers_rooms.id')
							 ->where('child_info.user_id',$id)
           					 ->select('childs_firstname','childs_lastname','teachers_name','room_no')
							 ->get();
			$totalchild= DB::table('child_info')->select(DB::raw('count(user_id) as child'))->where('child_info.user_id',$id)->get();
			$final=array('phone_no_1'=>$info->phone_no_1,'address'=>$address,'email'=>$info->email,'firstname'=>$info->firstname,'lastname'=>$info->lastname,'relation'=>$info->relationship_to_child,'total_child'=>$totalchild[0]->child);
		//	$a=array();
//			$b=array();
//			$i=0;
//			$c=array();
			//foreach($child_info as $child)	
//			 {
//				
//				$a[$i]=$child->childs_firstname;
//				$a[$i+1]=$child->childs_lastname;
//			 	$b[$i]=$a[$i]." ".$a[$i+1];
//				$c['name'.$i]=$b[$i];
//				
//				$i++;
//			  }
			 //$finalarray=array_merge($final,$c);
			 $finalarray=array_merge($final,$child_info);
			 return response()->json(json_encode($finalarray));
			
		}
		
		public function printpdf()
		{
		
		$mpdf=new mPDF('c','A4','','' , 0 , 0 , 0 , 0 , 0 , 0);
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
		$html = "<html>
		<body>
			<h1>Staff Details</h1>
			<table>
			<tr><th>Name</th><th>Designation</th></tr>
			<tr><th>Seema</th><th>Principal</th></tr>
			<tr><th>Rekha</th><th>Wise Principal</th></tr>
			<tr><th>Rishabh</th><th>Teacher</th></tr>
			<tr><th>Tarun</th><th>Teacher</th></tr>
			<tr><th>Payal</th><th>Teacher</th></tr>
			</table>
		</body>
		</html>";
		$mpdf->WriteHTML($html);
		$orderPdfName = "order-".$singleOrder[0]['display_name'];
		$mpdf->Output("school_detail.pdf",'I');
		header('Content-type: application/pdf');
		header("Content-Disposition: attachment; filename=school_detail.pdf");
	}
	public function getevent()
		{
		
				$events = array();
				
				$e = array();
				
				$e['title'] = "hello";
				$e['start'] ="2015-01-24T16:00:00+04:00";
				$e['backgroundColor'] = "#f56954";
				$e['borderColor'] =  "#f56954";
				//$allday = ($fetch['allDay'] == "true") ? true : false;
				//$e['allDay'] ="false";
			
				array_push($events, $e);
				//echo json_encode($events);
				//return view('welcome');
				return response()->json(json_encode($events));
			
				
	
			
		}
		public function chat($name,$id)
		{
		
			$data=DB::table('chats')->select('user_id','message')->orderBy('id','desc')->get();
						//print_r($data);
			return view('chat')->with('name',$name)->with('id',$id)->with('data',$data);
		//	insert into chat -----id,msg
		}
	
		public function savemsg(Request $request)
		{
	
	 
			//return response()->json(array('msg'=>"hello"));
			DB::table('chats')->insert([
    			'user_id' =>$request->userid,
    			'message' => $request->ChatText
						]);
				return Redirect::back();
			
		}
		
}
