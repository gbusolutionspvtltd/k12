@extends('apppage')
@section('content') 
<style>
#ChatBig{width:500px;height:450px;border:1px solid #000;margin:20px; }
#ChatMessages{width:300px;height:400px; padding-left:10px; padding-top:10px}
#ChatText{width:495px;height:48px;}
#ChatText:focus{outline:none;}
</style>
<section class="content-header">
          <h1>
           CHAT 
		                         <!--MAIN CONTENT WILL BE HERE! -->
          </h1>
          <ol class="breadcrumb" style="padding-right:250px">
            <li><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Chat</li>
          </ol>
        </section>
<section class="content">
<h3><span style="color:green">Welcome&nbsp;{{ $name }}</span></h3><br><br />

<div id="ChatBig" >
	<div id="ChatMessages">
	<table>
		@foreach($data as $text)
		<tr>
		<?php 
		$firstname=DB::table('users')->select('firstname')->where('id',$text->user_id)->first();
		
		?>
		
		
			<th>{{ $firstname->firstname }}</th>
			
			<td>{{ $text->message }}</td>
			
			</tr>
		@endforeach
		</table>
	</div>
	<form action="{{ url('/savemsg') }}">
			 {!! csrf_field() !!}
			 <input type="hidden" name="userid" value= {{$id}} >
	<textarea id="ChatText" name="ChatText" style="width:85%"></textarea>
	<button type="submit" class="btn btn-success" name="action" value="savemessage" style="width:15%; padding-right:20px; padding-bottom:10px; float:right">SEND</button>
	</form>
</div>	

</section>
<!--<script>
$(document).ready(function(){
	
	$("#ChatText").keyup(function(e){
	//When We Press Enter
		if(e.keyCode==13)
		{
		//alert("ds");
			var ChatText=$("#ChatText").val();
			$.ajax({
			type	:	'POST',
			url		:   '/savemsg',
			async	:	false,
			dataType:	'json',
			data	: ChatText,
           // headers: { 'X-CSRF-Token' : _token},
			success	:	function(data)
				{
				   alert(data);
				 },
			error: function (data) {
                console.log('Error:', data);
            }
			});
		}	
	});

});
</script>-->
@endsection
