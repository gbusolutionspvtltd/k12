////////////////////Home Controller
public function get_carPrice($tour_id,$car_id) {
//only respond to ajax calls
if(Request::ajax()){

$carPrice=DB::table(�car_prices�)
->join(�tours�, �car_prices.zone�, �=�, �tours.zone�)
->where(�car_prices.car_id�, �=�, $car_id)
->where(�tours.id�,�=�,$tour_id)
->select(�car_prices.price as price�,�tours.id as tour_id�,�car_prices.car_id as car_id�)
->get();

// array of objects are returned though in fact just one row(one field price) is returned
return $carPrice[0]->price;
}
}
///////////////////////Route
Route::get(�/tour/{id}/{car_id}�, array(�uses�=>�HomeController@get_carPrice�));

///////////////////jquery
jQuery(document).ready(function($) {

//************************************
// Car price based on zone
//************************************
//get value of field tour_id
var tour_id=$(�#tour_id�).val();
$(�#car_id�).change(function(e) { // e=event
// get select car_id
var car_id=$(�#car_id option:selected�).val();
// AJAX request
$.get(�/tour/�+tour_id+�/�+car_id, function(data) {
$(�#car-price�).text(�$�+data);

});

})

});

/////////////////view

<div class=�col-sm-4?>
{{ Form::select(�car_id�, $car_list,null, array(�id�=>�car_id�) ) }}
</div>

{{� Placeholder for AJAX content �}}
<div class=�col-sm-4?>
{{ Form::label(�car-price�, � �, array(�id�=>�car-price�, �class�=>�col-sm-2 control-label�,�style�=>�color:blue;font-weight: bold�)) }}
</div>